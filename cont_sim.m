function [y t] = cont_sim(A,x0,Nsim,T)
%CONT_SIM returns continuous simulation results for a system starting at
%t=0 from the initial point x0 and following the continuous time dynamics
%described by dx/dt=Ax, where A is an n-by-n matrix.
%
% Input Arguments:
% A     : A square matrix corresponding to the autonamous system's dynamics
% x0    : Initial state at t=0.
% Nsim : Simulation points; defines the resolution of the numerical
%         solution
% T     : Simulation Horizon
%
% Output Arguments:
% y     : An Nsim-by-nx vector with the state's trajectory where nx stands
%         for the dimension of the state vector.
% t     : An Nsim-by-1 vector with the corresponding times.
%
%
% How to plot the results:
% Here is an example on how to plot the results...
% [y t] = cont_sim(A,x0,Nsim,T);
% % Phase-space portrait...
% plot(y(1,:),y(2,:));
% % State evolution with time
% plot(t,y(1,:));
%
%SEE ALSO:
%imp_sim

nx=size(A,1);
y=zeros(nx,Nsim);
t=zeros(Nsim,1);
h=T/(Nsim-1);
for i=1:Nsim
    t(i)=(i-1)*h;
    y(:,i)=expm(A*t(i))*x0;
end