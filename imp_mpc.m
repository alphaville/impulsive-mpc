%IMP_MPC runs simulations for a closed-loop system with the control action
%of a stabilising impulsive MPC controller. This is a script to exemplify
%the use of the impulsive MPC toolbox. Before you run it, first run
%imp_Lithium or imp_loadExample1 or imp_loadExample2.
%
%See also
%imp_Lithium, imp_loadExample1 and imp_loadExample2.

%% Initialization
% Some initial definitions
N=10;%Prediction Horizon
ops.selectionType='cheby';
ops.verbose=2;
ops.solver='gurobi';
[controller, F] = imp_mpcController(N,A,B,T,Z,U,X,Y,V,[2 1],ops);

%% Simulations on the Closed-Loop
% Initial state
if exist('example','var') && isfield(example, 'name'),
    if strcmp(example.name,'lithium')==true,
        x0=[0.05; 0.05; 0.05];
    else
    end
else
    x0 = 1.5 * rand(size(A,1),1);
end

Nsim=50;% Number of simulation points (continuous time)
NContSim=30;%Simulation Horizon
U_=zeros(size(B,2),Nsim);
u_=controller{x0};
U_(:,1)=u_;
y=imp_sim(A,B,x0,NContSim,T,u_);
xplus=x0+B*imp_getInput(x0,controller,A,B,T,Z,U,Y,V,ops.selectionType);
Y_states=zeros(size(A,1),(NContSim+1)*(Nsim+1));
Y_states(:,1:NContSim+1)=y;
ComputationTimes = zeros(Nsim,1);
for i=1:Nsim
    x_=PhiT*xplus;%Current state
    tic;
    u_=imp_getInput(x_,controller,A,B,T,Z,U,Y,V,ops.selectionType);%MPC action
    ComputationTimes(i,1)=toc;
    y=imp_sim(A,B,x_,NContSim,T,u_);%Continuous time simulation
    Y_states(:,((1:NContSim+1)+i*(1+NContSim)))=y;
    U_(:,1+i)=u_;
    xplus=x_+B*u_;%State after the jump (at t+)
end
x_=PhiT*xplus;

%% Plot
%plot(Z)
hold on
%plot(Y,'y');
if size(A,1)==3,
    plot3(Y_states(1,:),Y_states(2,:),Y_states(3,:),'-')
    grid on;
    if exist('example','var') && isfield(example, 'name') && ...
            strcmp(example.name,'lithium')==true,
        xlabel('C_{pl}');
        ylabel('C_{RBC}');
        zlabel('C_{M}');
    end
elseif size(A,1)==2,
    plot(Z,'g');
    hold on;
    plot(Y,'y');
    plot(Y_states(1,:),Y_states(2,:),'-');
else
    disp(['[IMP_MPC] Phase-space plot not possible for state dimension: ' num2str(size(A,1))]);
end
axis tight

%% Plots
%TODO: Plot everything here
if exist('example','var') && isfield(example, 'name') && ...
        strcmp(example.name,'lithium')==true,
    figure(3);
    
    time_size = (NContSim+1)*(Nsim+1);
    time=zeros(1,time_size);
    
    time(1)=0;
    h=T/NContSim;
    time(2:NContSim+1)=0:h:(NContSim-1)*h;
    for i=1:Nsim
        time(i*(NContSim+1)+(1:NContSim+1))=...
            time((i-1)*(NContSim+1)+(1:NContSim+1))+T;
    end    
    
    % Plot the concentration-time profiles
    hold on;
    subplot(311);
    xi=[time(1) time(end)];
    yi_lo=0.4*[1 1];
    yi_hi=0.6*[1 1];
    fill([xi,fliplr(xi)],[yi_lo,fliplr(yi_hi)],'y');
    hold on;
    plot(time,Y_states(1,:),'b');
    axis tight;
    
    ylabel('C_{pl} (nmol/L)');
    subplot(312);
    xi=[time(1) time(end)];
    yi_lo=0.6*[1 1];
    yi_hi=0.9*[1 1];
    fill([xi,fliplr(xi)],[yi_lo,fliplr(yi_hi)],'y');
    hold on;
    plot(time,Y_states(2,:),'k');
    axis tight;
    
    ylabel('C_{RBC} (nmol/L)');
    subplot(313);
    xi=[time(1) time(end)];
    yi_lo=0.5*[1 1];
    yi_hi=0.8*[1 1];
    fill([xi,fliplr(xi)],[yi_lo,fliplr(yi_hi)],'y');
    hold on;
    plot(time,Y_states(3,:),'b');
    axis tight;
    ylabel('C_{M} (nmol/L)');
    xlabel('time (h)');
    
    % Plot the input
    set(0,'DefaultTextFontname', 'Helvetica')
    set(0,'DefaultTextFontSize', 12)
    set(0,'DefaultAxesFontSize', 12)
    stem((0:15)*T, U_(1:16)*10.9);
    xlabel('time (h)');
    ylabel('Dose (nmol)');
end
