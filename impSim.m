function y = impSim(A,B,x0,Nstep,delta,U)
%IMPSIM
% Simulates an impulsive system of the form x'=Ax (in continuous time) with
% the impulsive law Δx=B*u.
%
nx = size(A,1);
Hor = size(U,2);
Ad=cell(Nstep,1);
h=delta/Nstep;
for i=1:Nstep
    Ad{i}=expm(A*i*h);
end

y = zeros(nx,Nstep*Hor+1);
y(:,1)=x0;
for i=1:Hor
    y(:,(i-1)*(Hor-1)*Nstep+1)=y(:,(i-1)*(Hor-1)*Nstep+1)+B*U(:,i);
    for j=1:Nstep        
        y(:,(i-1)*(Hor-1)*Nstep+1+j)=Ad{j}*y(:,(i-1)*(Hor-1)*Nstep+1);
    end
end