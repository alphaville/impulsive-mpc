%IMP_LITHIUM This is a 3-D model for the pharmacokinetics of Lithium
%and it can be found in: Barbara E. Ehlich, Chris Clausen and Jared M.
%Diamond, Lithium Pharmacokinetics: Single Dose Experiments and Analysis
%Using a Physiological Model", Journal of Pharmacokinetics and
%Biopharmaceutics, 8(5), 1980, pp. 439-461.
%
%The system's states correspond to:
%x1: Plasma Concentration
%x2: RBC (Reb Blood Cells) Concentration
%x3: Muscle Concentration
%
%SEE ALSO:
%imp_mpc, imp_infset, imp_mpcController, imp_pre

clear;

example.name='lithium';

%PK parameters
V_p=10.9;  % Plasma volume
V_m=13.8;  % Muscles volume
V_c=2.5;   % RBC (reb blood cells) volume

k_a=0.36;
k_in_t=0.26;
k_ef_t=0.19;
k_in=0.29;
k_ef=0.80;

k_in_tilde=V_c*k_in/V_p;
k_in_t_tilde=V_m*k_in/V_p;
k_e_tilde=0.18;


% System Parameters
A=[ -k_e_tilde-k_in_tilde-k_in_t_tilde,  k_ef*V_c/V_p, k_ef_t*V_m/V_p
    k_in*V_p/V_c, -k_ef,   0
    k_in_t*V_p/V_m, 0, -k_ef_t];
B=[ 1
    0
    0];
T=3; % impulsive period
PhiT=expm(A*T);

n=3; % number of state variables
m=1; % number of inputs

%% Polytopic Overapproximation:
tic;
V=gnb1(A,0,T,10);
V_ComputationTime = toc;

%% State and Input Constraints:
Hx=[-eye(3)
    eye(3)];
Kx=[0
    0
    0
    2
    1.2
    1.2];
X=polytope(Hx,Kx); % state constraints in continuous time.
U=unitbox(1,1)*0.25+0.25; [Hu, Ku]=double(U); % Input constraints
Hz=[eye(3)
    -eye(3)]; 
Kz=[0.6
    0.9
    0.8
    -0.4
    -0.6
    -0.5];
% Z is the target-set
%just in case one defines Z to be not a subset of X...
Z=intersect(X,polytope(Hz,Kz));
[Hz, Kz]=double(Z);

% Computation of an impulsively control invariant set Y.
tic;
try
    Y=imp_infset(A,B,T,Z,U,V);
catch exception
    disp( exception.message );
    Y=polytope();
end
Y_ComputationTime = toc;
[Hy, Ky]=double(Y);
Hy=(abs(Hy)>1e-4).*Hy;
Y=polytope(Hy,Ky);
[Hy, Ky]=double(Y);