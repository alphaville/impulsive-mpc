function D=imp_augInvSet(A,B,T,Z,U,Y,V)
%Returns the set D of all augmented states ξ=(x,u) in R^{n+m} such that x∈Y
%and u∈Uf(x), where Uf(x) is the set of admissible inputs calculated using
%imp_adminp (given a state x∈Y). Note that the projection of D on X
%coincides with Y.
%
% Input Arguments:
% A,B : Matrices of the Impulsive System
% T   : Period between successive equidistant impulse times
% Z   : The target set as a polytope
% U   : The input constraints as a polytope.
% Y   : An impulsively control invariant set with respect to Z. This can be
%       calculated by means of imp_infset. 
% V   : V is the set of extreme points of a polyhedral set that includes
%       the matrices of the family {e^At; 0<t<T} as it is computed either
%       by gnb1 or hullJordExp. Example: V=gnb1(A,0,T,10);

PhiT=expm(A*T);
[Hu Ku]=double(U);
[Hz Kz]=double(Z);
[Hy Ky]=double(Y);

m=size(B,2);
[ny my]=size(Hy);
nu=size(Hu,1);

Hd=[Hy zeros(ny,m);
    Hy*PhiT Hy*PhiT*B;
    zeros(nu,my) Hu];
Kd=[Ky;Ky;Ku];
for i=1:length(V)
    Hd = [Hd;Hz*V{i} Hz*V{i}*B];
    Kd = [Kd;Kz];
end
D=polytope(Hd,Kd);
