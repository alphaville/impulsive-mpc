A=[-1 1;-5 -2];eig(A)
B=[1;2];
T=0.3;
    PhiT=expm(A*T);

% State and Input Constraints:
X=unitbox(2,1); [Hx Kx]=double(X);
U=unitbox(1,1); [Hu Ku]=double(U);
Hz=[1 0;-1 0;0 1;0 -1]; Kz=[0.2;0.2;0.9;0.9]; 
Z=intersect(X,polytope(Hz,Kz)+[0.75;0]);
[Hz Kz]=double(Z);

% Polytopic Overapproximation - S(x,u)
%V=hullJordExp(A,0,T);
V=gnb1(A,0,T,10);


Y=imp_infset(A,B,T,Z,U);

x=chebyball(Y);

Uf=imp_adminp(x,A,B,T,Z,U,Y,V);
u=0.12;
