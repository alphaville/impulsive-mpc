# Impulsive MPC Toolbox #

### Dependencies ###
* [MATLAB](http://www.mathworks.com/)
* [MPT3 Toolbox](http://people.ee.ethz.ch/~mpt/3/)
* [eat](https://github.com/alphaville/eat)

### Instructions ###

First load an example by running 

    imp_loadExample1 

or 

    imp_loadExample2 

You may define your own system - it is recommended to modify any of the example files provided herein.

Then run:

    imp_mpc

this will plot simulations from a given initial point using the action of the MPC controller we designed for impulsive systems.
Another interesting example is provided in `imp_Lithium` about a physiologically-based pharmacokinetic model (PBPK) for which an Impulsive MPC controller is designed.