function Uf = imp_adminp(x,A,B,T,Z,U,Y,V)
%IMP_ADMINP
%Returns the set of admissible inputs U_f for a given state that lies 
%inside the controlled invariant set Y. If x???Y, then the set Uf is non empty
%and 
%
% Input arguments:
% x   : The state for which Uf should be computed
% A,B : The matrices of the impulsive system
% T   : The period between two successive impulse times
% Z   : The target set in the phase space
% U   : The polytope of input constraints
% Y   : An impulsively controlled invariant set computed by imp_infset
% V   : V is the set of extreme points of a polyhedral set that includes
%       the matrices of the family {e^At; 0<t<T} as it is computed either
%       by gnb1 or hullJordExp. Example: V=gnb1(A,0,T,10);
%
% Output Arguments:
% Uf  : The set of admissible input in the sense that if x???Y then for any
%       u???Uf(x), x+bu???Z, Vi*(x+bu)???Z and e^{AT}(x+bu)???Y.
%
%SEE ALSO:
%imp_infset, imp_pre
%
%Author: Pantelis Sopasakis
if ~isinside(Y,x)
    error('ERROR: x is not inside the impulsively controlled invariant set Y');
end
if nargin==7
    V=gnb1(A,0,T,10);
end

PhiT=expm(A*T);
[Hu, Ku]=double(U);
[Hz, Kz]=double(Z);
[Hy, Ky]=double(Y);

H=[Hu;Hy*PhiT*B];
K=[Ku;Ky-Hy*PhiT*x];
for i=1:length(V)
    H=[H;Hz*V{i}*B];
    K=[K;Kz-Hz*V{i}*x];
end
Uf=polytope(H,K);