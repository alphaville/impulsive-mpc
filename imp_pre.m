function Pre = imp_pre(Y,A,B,T,X,U,V)
%IMP_PRE
%Returns the set of initial states that can be steered in one step into the
%set Y at the end of the period T between two impulses. In particular, it
%it is the set of initial states at time 0 for which there an input u∈U
%such that x+bu∈X, e^{At}(x+bu)∈X for all t∈(0,T] and e^{AT}(x+bu)∈Y.
%
% Input Argumets:
% Y   : The set for which Pre(Y) should be computed
% A,B : The matrices of the impulsive system
% T   : The time period between two successive impulses
% X   : The polytope (or polyhedron) of state constraints
% U   : Input constraints polytope/polyhedron
% V   : Set of matrices that are extreme points for a convex polytope
%       bounding {e^{At};t∈(0,T]}. It is returned by gnb1 or hullJordExp.
%
% Output Arguments
% Pre : The Pre set of Y.
%
%SEE ALSO:
%imp_infset, imp_adminp
%
%Author: Pantelis Sopasakis

if nargin==6
    V=gnb1(A,0,T,10);
end

[Hu Ku]=double(U);
[Hx Kx]=double(X);
[Hy Ky]=double(Y);

Hg=blkdiag(Hx,Hu);
Kg=[Kx;Ku];

PhiT=expm(A*T);
Hg=[Hg;[Hy*PhiT Hy*PhiT*B]];
Kg=[Kg;Ky];

for i=1:length(V)
    Hg = [Hg;
        Hx*V{i} Hx*V{i}*B];
    Kg = [Kg;
        Kx];
end

Pre=projection(polytope(Hg,Kg),[1:size(A,1)]);