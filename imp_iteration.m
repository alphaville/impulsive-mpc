imp_loadExample;

figure(1);
hold on;
plot(intersect(X,unitbox(2,20)),'g');
plot(intersect(Z,unitbox(2,20)),'y');
plot(intersect(Y,unitbox(2,20)),'c');

x=chebyball(Y);

Uf=imp_adminp(x,A,B,T,Z,U,Y,V);
u=chebyball(Uf);

xplus=x+B*u;
xfin=PhiT*xplus;
plot(xplus(1),xplus(2),'r *')
plot(x(1),x(2),'r o')
plot(xfin(1),xfin(2),'r d')
line([x(1);xplus(1)],[x(2);xplus(2)],'color','m','linewidth',2)
y=impSim(A,B,x,50,T,u);
plot(y(1,:),y(2,:),'m','LineWidth',2);