function [y, t]=imp_sim(A,B,x0,Nsim,T,U)
%IMP_SIM Open-loop simulation of the impuslive system (A,B) using a
%prescribed sequence of inputs U={u1,...,up}.
%
%Syntax:
% [y, t]=imp_sim(A,B,x0,Nsim,T,U)
%
%Input arguments:
%
%
%SEE ALSO:
%cont_sim
t=0;
y=x0;
for i=1:size(U,1)
    xplus=y(:,end)+B*U(i,:);
    [y_, t_]=cont_sim(A,xplus,Nsim,T);
    y=[y y_]; %#ok<*AGROW>
    t=[t;(i-1)*T+t_];
end