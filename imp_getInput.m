function u = imp_getInput(x,controller,A,B,T,Z,U,Y,V,inpSelection)
if nargin<=9
    inpSelection = 'cheby';
end
if isinside(Y,x)
    Uf = imp_adminp(x,A,B,T,Z,U,Y,V);
    if strcmp('cheby',inpSelection)
        u = chebyball(Uf);
        return;
    elseif strcmp('random',inpSelection)
        xtr = extreme(Uf);
        rnd = rand(size(xtr,1),1);
        rnd = rnd/sum(rnd);
        u=0;
        for i=1:size(xtr,1)
            u = u + rnd(i)*xtr(i,:);
        end
        return;
    end
else
    u = controller{x};
    u=u(1);
end