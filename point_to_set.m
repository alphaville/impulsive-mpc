function [d project] = point_to_set(x,P)
%POINT_TO_SET returns the distance between a point in the space and a given
%polytopic set
%
% Input Arguments
% x     : A point in R^n
% D     : A polytopic set
%
% Output arguments
% d       : The distance between x and D.
% project : The projection of x on D.
%
%SEE ALSO:
%imp_objective
if isinside(P,x)
    d=0;
    project=x;
else
    [Hp, Kp]=double(P);
    n=size(x,1);
    z=sdpvar(n,1);
    F=[Hp*z<=Kp];
    objective = (x-z)'*(x-z);
    solvesdp(F,objective);
    project=double(z);
    d=norm(x-project);
end