function [controller, F, objective, Sx, Su]= imp_mpcController(N,A,B,T,Z,U,X,Y,V,Q,ops)
%IMP_MPCGETINPUT Calculates the MPC controller given the system's matrices
%and the polytopic constraints X (state constraints),Z (target set),Y
%(impulsively control invariant set with respect to Z) and U (input
%constraints).
%
% Input Arguments:
% N   : Prediction Horizon
% A,B : Matrices of the Impulsive System in the form
%         dx/dt = Ax,
%         Delta x = Bu.
% T   : Period between successive equidistant impulse times
% Z   : The target set as a polytope (instance of Polyhedron)
% U   : The input constraints as a polytope.
% X   : The state constraints as a polytope.
% Y   : An impulsively control invariant set with respect to Z. This can be
%       calculated by means of imp_infset.
% V   : V is the set of extreme points of a polyhedral set that includes
%       the matrices of the family {e^At; 0<t<T} as it is computed either
%       by gnb1 or hullJordExp. Example: V=gnb1(A,0,T,10);
% Q   : This is a 1x2 vector which is used as a (simplistic) weight-matrix
%       for the objective function. The first element of the vector is used
%       to penalize the state and the second for the input.
%
% Output Arguments
% controller : This object can be used to calculate the input action given
% any feasible state. It is used like a cell: controller{x}. For example:
% > controller = imp_mpcController(N,A,B,T,Z,U,X,Y,V);
% > x = [0.1;0.2];
% > u = controller{x};
%
%
%SEE ALSO:
%imp_infset, imp_augInvSet, imp_pre

narginchk(8, 11);
if nargin==8 || (nargin>=9 && isempty(V))
    V=gnb1(A,0,T,15); %Use default V
end
if nargin<=9 || (nargin>=10 && isempty(Q))
    Q=[1;2];
end
if nargin<11 || (nargin>=11 && isempty(ops))
    ops.verbose=2;
    ops.solver='fmincon';
    disp('XXX');
end

n=size(A,1);
m=size(B,2);
PhiT=expm(A*T);
Ad=PhiT;
Bd=PhiT*B;
D=imp_augInvSet(A,B,T,Z,U,Y,V);
[Hd, Kd]=double(D);
[Hu, Ku]=double(U);
[Hy, Ky]=double(Y);
[Hx, Kx]=double(X);
%
% Here, we construct the matrices for MPC
% [Batch formulation]
%
Sx=zeros(n*(N+1),n);
Sx(1:n,:)=eye(n);
for i=2:N+1
    Sx(1+(i-1)*n:i*n,:)=Sx(1+(i-2)*n:(i-1)*n,:)*Ad;
end
Su=zeros(n*(N+1),m*N);
for j=1:N,
    Acum=eye(n);
    for k=j:-1:1,
        Su(n+1+n*(j-1):n+n*j,1+m*(k-1):k*m)=Acum*Bd;
        Acum=Acum*Ad;
    end
end
    
%
% We now define the variables of the problem:
%
Uo=sdpvar(m*N,1); %Uo=[u0,u1,...,u(N-1)] length=N
Vo=sdpvar(m*N,1);
Zo=sdpvar(n*N,1);
x=sdpvar(n,1);
Yo = Sx*x+Su*Uo;

%
% Imposition of the constraints...
%
F=set([]);
for i=1:N
    F = [F; Hx*Yo((i-1)*n+1:n*i) <= Kx]; % State Constraints at every instant
    F = [F; Hd*[Zo((i-1)*n+1:n*i);Vo(i)] <= Kd]; % Auxiliary variables to be in D
    F = [F; Hu*Uo(i)<=Ku]; % Input Constraints
    %#ok<*AGROW>
    for j=1:length(V)
        F = [F; Hx*real(V{j})*(Yo((i-1)*n+1:n*i)+B*Uo(i)) <= Kx]; %#ok<*AGROW>
    end
end

F = [F; Hy*Yo((N-1)*n+1:n*N)<=Ky]; % Terminal Constraint (x(N) in Y)
objective = ...
    Q(1)*(Yo(1:end-n)'*Yo(1:end-n) + Zo'*Zo -2*Yo(1:end-n)'*Zo) +...
    Q(2)*(Uo'*Uo  + Vo'*Vo - 2*Uo'*Vo);
controller = optimizer(F,objective,...
    sdpsettings('verbose',ops.verbose,'solver',...
    ops.solver),x,Uo(1));
end