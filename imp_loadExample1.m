%IMP_LOADEXAMPLE1 returns a contlrolled invariant set Yf for an example
%linear impulsive system.
%
%SEE ALSO:
%imp_mpc, imp_infset, imp_mpcController, imp_pre

clear;

example.name = 'example1';

% System Parameters
A=[-1 1;-5 -2];
B=[1;2];
T=0.3;
PhiT=expm(A*T);

%% Polytopic Overapproximation:
V=gnb1(A,0,T,8);

%% State and Input Constraints:
X=unitbox(2,1); [Hx, Kx]=double(X);
U=unitbox(1,1); [Hu, Ku]=double(U);
Hz=[1 0;-1 0;0 1;0 -1]; Kz=[0.2;0.2;0.9;0.9]; 
Z=intersect(X,polytope(Hz,Kz)+[0.75;0]);
[Hz, Kz]=double(Z);

%% Computation of an impulsively control invariant set Y.
try
    Y=imp_infset(A,B,T,Z,U,V);
catch exception
    disp( exception.message );
    Y=polytope();
end