function obj = imp_objective(U,x,N,D,Ad,Bd)
obj = 0;
for i=1:N
    u=U(i);
    obj = obj + point_to_set([x;u],D);
    x = Ad*x + Bd*u;    
end