%IMP_KRAMERETAL74 PK Model by Kramer et al.
%
%SEE ALSO:
%imp_mpc, imp_infset, imp_mpcController, imp_pre

clear;

%PK parameters
V_p=11.3;
V_m=13.1;
V_c=2.5;

k_a=1.88;%OK
k_in_t=0.38;
k_ef_t=0.30;
k_in=0.12;
k_ef=0.35;

k_in_tilde=V_c*k_in/V_p;
k_in_t_tilde=V_m*k_in/V_p;
k_e_tilde=0.14;


% System Parameters
A=[ -k_a-k_e_tilde-k_in_tilde-k_in_t_tilde    k_ef*V_c/V_p       k_ef_t*V_m/V_p
    k_in*V_p/V_c    -k_ef   0
    k_in_t*V_p/V_m  0       -k_ef_t];
B=[ 1
    0
    0];
T=0.2;
PhiT=expm(A*T);

%% Polytopic Overapproximation:
V=gnb1(A,0,T,24);

%% State and Input Constraints:
Hx=[-eye(3)
    eye(3)];
Kx=[0
    0
    0
    2
    2
    2];
X=polytope(Hx,Kx);
U=unitbox(1,1)*10; [Hu Ku]=double(U);
Hz=[eye(3)
    -eye(3)]; 
% Kz=[2
%     2
%     2
%     -1
%     -1.1
%     -1.1]; 
Kz=[1.8
    1.6
    1.8
    -0.5
    -1.2
    -1];
Z=intersect(X,polytope(Hz,Kz));%just in case...
[Hz Kz]=double(Z);

% Computation of an impulsively control invariant set Y.
try
    Y=imp_infset(A,B,T,Z,U,V);
catch exception
    disp( exception.message );
    Y=polytope();
end