%IMP_LOADEXAMPLE2 returns a contlrolled invariant set Yf for an example
%linear impulsive system.
%
%SEE ALSO:
%imp_mpc, imp_infset, imp_mpcController, imp_pre

clear;

% System Parameters
A=[-1 1;-5 -2];
B=[1;2];
T=0.3;
PhiT=expm(A*T);

%% Polytopic Overapproximation:
V=gnb1(A,0,T,15);

%% State and Input Constraints:
X=unitbox(2,1); [Hx Kx]=double(X);
U=unitbox(1,1); [Hu Ku]=double(U);
Hz=[   1   0.2
      -1   0
       0   1
       -0.1  -1
      -1   1]; 
Kz=[ +0.8
     -0.45
     +0.9
     +0.7
     -0.3 ];
Z=intersect(X,polytope(Hz,Kz));
[Hz Kz]=double(Z);
plot(X,'w');
hold on;
plot(Z,'y');

% Computation of an impulsively control invariant set Y.
try
    Y=imp_infset(A,B,T,Z,U,V);
    plot(Y,'g');
catch exception
    disp( exception.message );
    Y=polytope();
end