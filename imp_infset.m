function Y = imp_infset(A,B,T,Z,U,V)
%IMP_INFSET
%Returns an impulsively controlled invariant set for a given linear
%impulsive system with respect to a given target set. The system is subject
%to state and input constraints. The impulsive instants are considered to
%be equidistant.
%
% Input Arguments:
% A and B : The matrices of the linear impulsive system
% T : The impulsive period.
% Z : The target set which should be a subset of the set of state
%     constraints.
% U : The set of input constraints.
% V   : V is the set of extreme points of a polyhedral set that includes
%       the matrices of the family {e^At; 0<t<T} as it is computed either
%       by gnb1 or hullJordExp. Example: V=gnb1(A,0,T,10);
%
% Output:
% Y : The impulsively controlled invariant set with respect to the given
%     target set Z. Each state that is found in Y at some impulsive instant
%     will return inside Y right before the next impulse is to be applied
%     and the whole trajectory of the system in continuous time will be
%     bound inside Z.
%
%SEE ALSO:
%imp_adminp, imp_pre
%
%Author: Pantelis Sopasakis

PhiT=expm(A*T);

% State and Input Constraints:
[Hu, Ku]=double(U);
[Hz, Kz]=double(Z);

% Polytopic Overapproximation - S(x,u)
if nargin==5
    V=gnb1(A,0,T,10);
end

% Initial set: G ??? Z??U
Hg=blkdiag(Hz,Hu);
Kg=[Kz;Ku];
G=polytope(Hg,Kg); % G = Z??U

%    y+B???u ??? Z
% ??? Hz???(y+B???u) ??? Kz
% ??? Hz???y+Hz???B???u ??? Kz
Qz1=[Hz Hz*B]; Qz2=Kz;

% y(t) ??? Z ??? S(x,u) ??? Z
for j=1:length(V)
    Qz1=[Qz1; Hz*V{j} Hz*V{j}*B];
    Qz2=[Qz2;Kz]; %#ok<*AGROW>
end


for i=1:500
    % disp(['iter: ' num2str(i)]);
    if i>=2
        [xCheby_old, rCheby_old]=chebyball(Y);
    end
    Y=projection(G,1:size(A,1)); % Y = proj_x(G)
    [Hy, Ky]=double(Y);
    if isempty(Hy)       
        error('ERROR: Algorithm converged to the empty set');
    end
    if i>=2
        [xCheby, rCheby]=chebyball(Y);
        err = norm(xCheby_old-xCheby)+(rCheby_old-rCheby)^2;
        if err<1E-8
            break;
        end
    end
    Hg=[Hy zeros(size(Hy,1),1);
        Hy*PhiT Hy*PhiT*B;
        Qz1];
    Kg=[Ky;
        Ky;
        Qz2];
    G=polytope(Hg,Kg);
end
%disp(['INFO: Algorithm Converged after ' num2str(i) ' iterations.']);
